# Orchestration de la stack Elastic (ELK) sur K8S

1. Création de la configmap elk : ```kubectl apply -f elk-config.yml```
2. Création du déploiement elk : ```kubectl apply -f elk.yml```
2. Création du service elk (pour exposer les ports): ```kubectl apply -f elk-service.yml```

*__Note__* : N'oubliez pas de changer l'hôte de elasticsearch sur les fichiers de configuration de kibana et de logstash selon l'adresse de elasticsearch.
